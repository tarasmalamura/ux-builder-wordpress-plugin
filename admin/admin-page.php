<?php
$tab = $_GET['tab'] ?? 'app';
$pages = ux_builder()->get_option('enabled') ? ux_builder()->getFirebasePages() : null;
?>
<div class="wrap ux_builder_wrap">
    <h2><?= get_admin_page_title() ?></h2>
    <hr>

    <nav class="nav-tab-wrapper">
        <a data-tab="app" class="nav-tab <?= $tab == 'app' ? 'nav-tab-active' : '' ?>">App</a>
        <?php if($pages !== null): ?>
            <a data-tab="pages" class="nav-tab <?= $tab == 'pages' ? 'nav-tab-active' : '' ?>">Page settings</a>
        <?php endif; ?>
    </nav>

    <div class="tab-content-wrapper">

        <div class="tab-content <?= $tab == 'app' ? '' : 'hidden' ?>" data-tab="app">
            <form method="post" class="ux_builder_settings_form">

                <?php wp_nonce_field('ux_builder_settings_nonce', 'ux_builder_settings_nonce'); ?>
                <input type="hidden" name="section" value="app">

                <table class="form-table">
                    <tr>
                        <th>Enabled</th>
                        <td>
                            <select name="ux_builder_settings[enabled]">
                                <option value="0">No</option>
                                <option value="1" <?= ux_builder()->get_option('enabled') ? 'selected' : '' ?>>Yes</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>App ID</th>
                        <td><input name="ux_builder_settings[app_id]" value="<?= ux_builder()->get_option('app_id') ?>" type="text" class="regular-text"></td>
                    </tr>
                    <tr>
                        <th>App name</th>
                        <td><input name="ux_builder_settings[app_name]" value="<?= ux_builder()->get_option('app_name') ?>" type="text" class="regular-text"></td>
                    </tr>
                    <tr>
                        <th>Author ID</th>
                        <td><input name="ux_builder_settings[author_id]" value="<?= ux_builder()->get_option('author_id') ?>" type="text" class="regular-text"></td>
                    </tr>
                </table>
                <p class="submit"><input type="submit" class="button button-primary" value="Save changes"></p>
            </form>
        </div>

        <div class="tab-content <?= $tab == 'pages' ? '' : 'hidden' ?>" data-tab="pages">
            <form method="post" class="ux_builder_settings_form">

                <?php wp_nonce_field('ux_builder_settings_nonce', 'ux_builder_settings_nonce'); ?>
                <input type="hidden" name="section" value="pages">

                <table class="form-table">
                    <?php if($pages !== null): ?>
                        <tr>
                            <th>Publish articles to page</th>
                            <td>
                                <select name="ux_builder_settings[page_id]">
                                    <option value="0"> - </option>
                                    <?php foreach($pages AS $p):?>
                                        <option value="<?= $p->id ?>" <?= $p->id == ux_builder()->get_option('page_id') ? 'selected' : '' ?>><?= $p->menuTitle ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>
                    <?php endif; ?>
                </table>
                <p class="submit">
                    <?php if($pages): ?>
                        <input type="submit" class="button button-primary" value="Save selected page" >
                    <?php endif; ?>
                </p>
            </form>
        </div>
    </div>

</div>