<?php

if(!function_exists('print_array')){
    function print_array($arr = [], $die = false)
    {
        printf('<pre>%s</pre>', print_r($arr, true));
        if($die) die();
    }
}

