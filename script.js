function ux_builder_scripts($){

    var $body = $('body');

    function addNotice(message, status)
    {
        $body.find('form.ux_builder_settings_form:visible').prepend('<div class="notice notice-'+status+' is-dismissible"><p>'+message+'</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Close</span></button></div>')
    }

    $body.on('click', '.notice-dismiss', function (){
        $(this).closest('.notice').remove();
    })

    $body.on('click', '.ux_builder_wrap .nav-tab', function(){
        var $wrapper = $(this).closest('.ux_builder_wrap');
        var tab = $(this).data('tab');
        $wrapper.find('.nav-tab').removeClass('nav-tab-active');
        $(this).addClass('nav-tab-active');
        $wrapper.find('.tab-content').addClass('hidden');
        $wrapper.find('.tab-content[data-tab="'+tab+'"]').removeClass('hidden');
    });

    $body.on('submit', 'form.ux_builder_settings_form', function(){

        var $form = $(this);
        var data = $form.serialize();
        data += '&action=yf_ajax_ux_builder_save_settings';

        $.ajax({
            type: "POST",
            url: "/wp-admin/admin-ajax.php",
            dataType: "json",
            data: data,
            beforeSend: function () {
                $form.find('[type=submit]').attr('disabled', 'disabled');
            },
            complete: function () {
                $form.find('[type=submit]').removeAttr('disabled');
            },
            success: function (json) {
                addNotice(json.message, json.status);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $form.find('[type=submit]').removeAttr('disabled');
                console.error(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });

        return false;
    });

}
ux_builder_scripts(jQuery);