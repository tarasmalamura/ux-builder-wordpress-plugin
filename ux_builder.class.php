<?php

class UX_Builder
{

    public $plugin_slug = 'ux-builder';

    protected $log_lines_limit = 50;

    private static $init = null;

    public static function init()
    {
        if (self::$init === null)
            self::$init = new self();
        return self::$init;
    }


    function __construct()
    {
        add_action( 'wp_ajax_yf_ajax_ux_builder_save_settings', [$this, 'ajax_save_settings'] );
        add_action( 'wp_ajax_yf_ajax_ux_builder_ajax_import_page', [$this, 'ajax_import_page'] );
        add_action( 'publish_post', [$this, 'publish_post'], 10, 3 );
    }

    private function curl($url, $method = 'post', $data = [])
    {
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Basic '. base64_encode(sprintf("%s:%s", $this->get_option('app_id'), $this->get_option('author_id')))
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);

        if($method == 'post'){
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }

        $result = curl_exec($ch);
        curl_close($ch);

        return @json_decode($result) ;
    }



    private function log($msg)
    {
        $log_path = plugin_dir_path(__FILE__).'/recent.log';
        $lines = file_exists($log_path) ? (explode(PHP_EOL, file_get_contents($log_path)) ?: []) : [];
        $lines[] = '['.date('Y-m-d H:i:s').'] '.$msg;
        file_put_contents($log_path, implode(PHP_EOL, array_slice( $lines, ($this->log_lines_limit*(-1)))));
    }

    private function die_json($arr = [])
    {
        die(json_encode($arr, JSON_UNESCAPED_SLASHES));
    }


    private $_settings;
    public function get_settings()
    {
        if($this->_settings !== null) return $this->_settings;
        $defaults = [
            'enabled' => 0,
            'app_id' => 0,
            'app_name' => '',
            'author_id' => 0,
            'page_id' => 0
        ];
        $settings = array_merge($defaults, get_option('ux_builder_settings') ?: []);
        return $this->_settings = $settings;
    }

    public function get_option($key)
    {
        return $this->get_settings()[$key] ?? null;
    }

    public function ajax_save_settings()
    {

        $json = [
            'status' => 'error',
            'message' => 'Unknown error'
        ];

        if(!wp_verify_nonce($_REQUEST['ux_builder_settings_nonce'] ?? '', 'ux_builder_settings_nonce')){
            $json['message'] = 'Form key error. Try to refresh the page';
            $this->die_json($json);
        }

        $this->_settings = array_merge($this->get_settings(), $_REQUEST['ux_builder_settings'] ?? []);
        update_option('ux_builder_settings', $this->_settings);

        $json['status'] = 'success';
        $json['message'] = 'The data is successfully saved';
        //$json['dd'] = $this->_settings;

        $this->die_json($json);
    }


    private function content_prepare($content = ''){
        $content = apply_filters( 'the_content', $content );
        $content = str_replace( ']]>', ']]&gt;', $content );
        return $content;
    }

    public function publish_post(int $post_id, WP_Post $post, string $old_status)
    {

        if(!$this->get_option('enabled') || $post->post_status == $old_status) return;

        $log_arr = [];
        $log_arr[] = 'action=export';
        $log_arr[] = 'cmsID='.$post_id;

        try{

            $response = $this->curl('https://us-central1-app-builder-stage.cloudfunctions.net/api/blog/publish', 'post', [
                'title' => $post->post_title,
                'content' => $this->content_prepare($post->post_content),
                'app' => $this->get_option('app_name'),
                'cmsID' => $post->ID,
                'image' => get_the_post_thumbnail_url($post->ID) ?: '',
                'publishDate' => $post->post_date,
                'link' => get_permalink($post->ID)
            ]);

            $firebaseID = 0;
            if(isset($response->status) && $response->status == 'Saved'){
                $firebaseID = $response->id ?? 0;
            }

            $log_arr[] = 'firebaseID='.$firebaseID;
        }
        catch (Exception $e){
            //
        }

        $this->log(implode('; ', $log_arr));
    }

    private $_firebase_pages;
    public function getFirebasePages()
    {
        if($this->_firebase_pages !== null) return $this->_firebase_pages;

        if(!$this->get_option('enabled')){
            return $this->_firebase_pages = [];
        }

        $pages = $this->curl('https://us-central1-app-builder-stage.cloudfunctions.net/api/list/pages', 'get');

        return is_array($pages) ? $this->_firebase_pages = $pages : [];
    }

}

function ux_builder(){
    return UX_Builder::init();
}