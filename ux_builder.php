<?php
/*
Plugin Name: UX Builder
Plugin URI: https://yourfuture.com.ua/
Description: UX Builder WP plugin
Author: YourFuture
Version: 0.3
Author URI: https://yourfuture.com.ua/
*/


require_once plugin_dir_path(__FILE__) . '/functions.php';
require_once plugin_dir_path(__FILE__) . '/ux_builder.class.php';
ux_builder();

add_action('admin_menu', 'admin_menu__ux_builder');
function admin_menu__ux_builder() {
    add_submenu_page(
        'tools.php',
        'UX Builder',
        'UX Builder',
        'manage_options',
        ux_builder()->plugin_slug,
        'admin_menu__ux_builder__callback'
    );
}

function admin_menu__ux_builder__callback() {
    include_once plugin_dir_path(__FILE__).'/admin/admin-page.php';
}

function load_custom_wp_admin_style_ux_builder()
{
    wp_enqueue_style('yf-ux_builder-style', plugin_dir_url(__FILE__) . '/style.css');
    wp_enqueue_script('yf-ux_builder-script', plugin_dir_url(__FILE__) . '/script.js', [], false, true);
}
if(isset($_GET['page']) && $_GET['page'] == ux_builder()->plugin_slug) {
    add_action('admin_enqueue_scripts', 'load_custom_wp_admin_style_ux_builder');
}

add_filter( 'plugin_action_links_ux_builder/ux_builder.php', 'ux_builder_settings_link' );
function ux_builder_settings_link( $links ) {
    $url = esc_url( add_query_arg(
        'page',
        'ux-builder',
        get_admin_url() . 'admin.php'
    ) );
    $settings_link = "<a href='$url'>" . __( 'Settings' ) . '</a>';
    array_push(
        $links,
        $settings_link
    );
    return $links;
}